/*
 * sensor.c
 *
 *  Created on: Nov 13, 2020
 *      Author: Zachary Whitlock
 */
#include "sensor.h"

int initAccel(int fileDesc) {
	// Register 0x6B - power management 1
	// Register 3B - AccelX Upper
	// Register 3C - AccelX Lower

	// Disable IC sleep mode
	char buf[2]; // Address + Data = 4
	buf[0] = 0x6B;
	buf[1] = 0x00; //0x43;
	if (write(fileDesc, buf, 2) != 2) {
		printf("Failed to write to device!\n");
	}


	buf[0] = 0x6B;
	buf[1] = 0x00; //0x43;
	if (write(fileDesc, buf, 2) != 2) {
		printf("Failed to write to device!\n");
	}

	// Init the i2c device and print out some accel data
	// set clock source
	// set fullscale gyro range
	// set fullscale accel range
	// set sleep disabled (power reg 1)
	return 0;
}

// Read two bytes, if sent the first address of an axis, it will return the axis value.
int readAxis(int fileDescriptor, char regAddr){
	int retrievedValue;
	char readBytes[2];

	// Write our register address
	if (write(fileDescriptor, &regAddr, 1) != 1) {
		printf("Failed to write to device!\n");
	}

	// Makes use of the rolling register pointer inside the accelerometer
	read(fileDescriptor, readBytes, 2);
	retrievedValue = (int16_t)((readBytes[0] << 8) | readBytes[1]);
	return retrievedValue;
}


// Write a register over the I2C bus
int writeRegister(int fileDescriptor, char regAddr, char data) {
	char buf[2];
	buf[0] = regAddr;
	buf[1] = data;

	// Write to register
	if (write(fileDescriptor, buf, 2) != 1) {
		printf("Failed to write to device!\n");
		return -1;
	}

	return 0;
}

// Read a register from the I2C device
int readRegister(int fileDescriptor, char regAddr) {
	int retrievedValue;
	char readBytes[1];
	char buf[1];
	buf[0] = regAddr;

	// Write our register address
	if (write(fileDescriptor, buf, 1) != 1) {
		printf("Failed to write to device!\n");
	}

	// Read value
	read(fileDescriptor, readBytes, 1);
	retrievedValue = (int8_t)(readBytes[0]);

	return retrievedValue;
}


