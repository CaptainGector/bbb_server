#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iobb.h>

// Network Code
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

// My libraries
#include "motor.h"
#include "sensor.h"
#include "network.h"


// Steps per second
// Is probably realistically limited to about 1000
int stepper_speed = -1000;

// Close up the server.
int closeUp = 0;

// Balancing act
int balance = 0;
const int targetY = 0;

const char filename[20] = "/dev/i2c-2";

// An overcomplicated approach originating from an nearly 24hrs without sleep.
// Takes a pointer to a string
// Takes the length of the string
// Returns an integer derived from the string fed to it.
int getIntFromString(char *str, int len) {
	int countLast;
	char *ptr;
	char dest[len];
	memset(dest, 0, sizeof(dest));

	// Find the first number
	int count = 0;
	while (str[count] != 0) {
		if ((str[count] >= 48 && str[count] <= 57) || str[count] == 45) {
			break;
		}
		count++;
	}

	// Count now contains the position of the first number
	if (count != len-1) {
		// ... now find the last number...
		countLast = count;
		while (str[countLast] != 0) {
			if ((str[countLast] >= 48 && str[countLast] <= 57) || str[countLast] == 45) {
				countLast++;
				continue;
			}
			break;
		}

		if ((countLast-count) <= len+1) {
			// Success!
			strncpy(dest, str+count, countLast-count);
			return (int)strtol(dest, &ptr, 10);
		} else {
			return 0;
		}
	}
	return 0;
}

// Command to handle the parsing of functions.
void parseCommand(char *command, int file, int nextClient) {
	int aVal;

	if ((strstr(command, "ACCEL READ")) != NULL) {
		if ((strstr(command, "X")) != NULL) {
			aVal = readAxis(file, 0x3B);
			sprintf(command, "ACCEL X %d\n", aVal);
			sendToClient(nextClient, command, strlen(command));
		} else if ((strstr(command, "Y")) != NULL) {
			aVal = readAxis(file, 0x3D);
			sprintf(command, "ACCEL Y %d\n", aVal);
			sendToClient(nextClient, command, strlen(command));
		} else if ((strstr(command, "Z")) != NULL) {
			aVal = readAxis(file, 0x3F);
			sprintf(command, "ACCEL Z %d\n", aVal);
			sendToClient(nextClient, command, strlen(command));
		} else {
			// Wrong format
			sprintf(command, "ERR FORMAT");
			sendToClient(nextClient, command, strlen(command));
		}
	} else if ((strstr(command, "GYRO READ")) != NULL) {
		// Remove the "Y" in gyro.
		char *ptr;
		ptr = strstr(command, "Y");
		ptr[0] = ' ';

		// Handle the command and send back the gyroscope data
		if ((strstr(command, "X")) != NULL) {
			aVal = readAxis(file, 0x43);
			sprintf(command, "GYRO X %d\n", aVal);
			sendToClient(nextClient, command, strlen(command));
		} else if ((strstr(command, "Y")) != NULL) {
			aVal = readAxis(file, 0x45);
			sprintf(command, "GYRO Y %d\n", aVal);
			sendToClient(nextClient, command, strlen(command));
		} else if ((strstr(command, "Z")) != NULL) {
			aVal = readAxis(file, 0x47);
			sprintf(command, "GYRO Z %d\n", aVal);
			sendToClient(nextClient, command, strlen(command));
		} else {
			// Badly formatted.
			sprintf(command, "ERR FORMAT");
			sendToClient(nextClient, command, strlen(command));
		}
	} else if ((strstr(command, "TEMP READ")) != NULL) {
		// Will send back the degrees in celcius * 100;
		aVal = readAxis(file, 0x41);
		aVal = (double)(aVal/340 + 36.53)*100;
		sprintf(command, "TEMP %d\n", aVal);
		sendToClient(nextClient, command, strlen(command));

	} else if ((strstr(command, "STEPPER")) != NULL) {
		// Stop moving.
		stepper_speed = 0;

		// Handle the command and send back 'OK' if sucess.
		if ((strstr(command, "CCW")) != NULL){
			balance = 0;
			aVal = getIntFromString(command, strlen(command));
			stepMotorN(aVal, 0); // '0' direction means counter clockwise.
			sendToClient(nextClient, "OK\n", strlen("OK\n"));
		} else if ((strstr(command, "CW")) != NULL) {
			balance = 0;
			aVal = getIntFromString(command, strlen(command));
			stepMotorN(aVal, 1); // '0' direction means counter clockwise.
			sendToClient(nextClient, "OK\n", strlen("OK\n"));
		} else if ((strstr(command, "SPEED")) != NULL){
			balance = 0;
			aVal = getIntFromString(command, strlen(command));
			stepper_speed = aVal;
			sendToClient(nextClient, "OK\n", strlen("OK\n"));
		} else if ((strstr(command, "BALANCE")) != NULL){
			// Toggle balance state
			if (balance != 0)
				balance = 0;
			else
				balance = 1;
			sendToClient(nextClient, "OK\n", strlen("OK\n"));
		}
	} else if ((strstr(command, "CLOSE")) != NULL) {
		closeUp = 1;
		sendToClient(nextClient, "CLOSE\n", strlen("CLOSE\n"));
	} else {
		sendToClient(nextClient, "UNRECOGNIZED\n", strlen("UNRECOGNIZED\n"));
	}
}


int main(int argc, char *argv[]) {
	int proportionalSpeed;

	int runTime = 20; // in seconds
	if (argc >= 2) {
		runTime = strtol(argv[1], NULL, 10);
	}

	int file = open(filename, O_RDWR);

	// This was interesting. The output of the thing
	// wasn't working with the sleep function being used and not
	// filling up the buffer.
	setbuf(stdout, NULL);

	// Initialize GPIO and turn on a pin..
    iolib_init();
    iolib_setdir(8, 8, DigitalOut);
    iolib_setdir(8, 10, DigitalOut);
    iolib_setdir(8, 12, DigitalOut);
    iolib_setdir(8, 14, DigitalOut);

    // Step the stepper driver 180 degrees
    // with step8, it requires 512 to rotate 360 degrees
    printf("Moving motor...");
    stepMotorN(1, 0); // '0' direction means counter clockwise.
    printf("Done moving.\n");

	// Specify device address for the file access
	if (ioctl(file, I2C_SLAVE, 0x68) < 0) {
		printf("Error setting address");
	}

	// Verify the file opened successfully.
	if (file < 0) {
		printf("Error opening file\n");
		return -1;
	} else {
		printf("File Opened!\n");
	}

	// Initialize the accelerometer
	printf("Initializing...\n");
	initAccel(file);
	printf("Initialized!\n");

	// Print out 50 X values
	int xVal;
	printf("Reading X values: \n");
	for (int x = 0; x < 10; x++) {
		xVal = readAxis(file, 0x3B);
		printf("%d\n", xVal);
		usleep(100000);
	}

	// Get a network connection
	setupListener();

	// Handle our clients! (20sec timeout)
	struct timeval oldTime;
	struct timeval nowTime;
	gettimeofday(&oldTime, NULL);
	gettimeofday(&nowTime, NULL);
	char buffer[32];
	int nextClient = 0;
	while ((nowTime.tv_sec - oldTime.tv_sec) < runTime) {
		gettimeofday(&nowTime, NULL);

		handleClients();
		memset(buffer, 0, strlen(buffer));

		// Petition to change 'nextClient' to 'nextCommand'....
		nextClient = getNextCommand(buffer);
		if (nextClient > 0) {
			parseCommand(buffer, file, nextClient);
			popCommand(nextClient);
		}

		if (balance != 0) {
			// Calculate the proportional speed
			int currentY = readAxis(file, 0x3D);
			proportionalSpeed = targetY - currentY;

			// Set min/max speeds
			if (abs(proportionalSpeed) > 2000) {
				if (proportionalSpeed < 0)
					proportionalSpeed = -2000;
				else
					proportionalSpeed = 2000;

			} else if(abs(proportionalSpeed) < 500) {
				if (proportionalSpeed < 0)
					proportionalSpeed = -500;
				else
					proportionalSpeed = 500;
			}

			stepper_speed = proportionalSpeed;
		}

		if (stepper_speed != 0) {
			if (stepper_speed > 0)
				stepMotor(1);
			else
				stepMotor(0);

			usleep(1000000/(abs(stepper_speed)));
		} else {
			// Just sleep for a ms
			usleep(1000);
			// Probably not necessary with this loop structure.
			// Might lessen CPU load?
		}
		if (closeUp)
			break;
	}


	// Print out all the commands in the buffer (Should be empty)
	printCommands();

	// Close the I2C file.
	close(file);

	return 0; // Success!
}
