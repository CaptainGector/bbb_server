/*
 * network.c
 *
 *  Created on: Nov 13, 2020
 *      Author: Zachary Whitlock
 */

#include "network.h"

/* (Design psuedo code \/) --------------------------------------
 * First, setup a server socket for clients to connect to.
 *
 * Second, poll for new clients
 *
 * Third, check for new messages
 *
 * Fourth, talk back to clients that requested something
 *
 * --------------------------------------------------------------
 *
 * It should be noted that the current design only allows a single
 * command per client to be stored in the buffer. This should be of no
 * issue since the code spends the majority of time handling the networking
 * side of things. If other operations get more intensive, it's possible that
 * a client could send another command and not get the previous one processed.
 *
 * To allow a client to have multiple commands, all that has to be done is to
 * make a new unique identifier. Coming up with the unique identifier can be as
 * simple as copying and modifying the file descriptor every new command.
 *
 */

// Send a response to a client
int sendToClient(int fd, char *str, int len) {
	send(fd, str, len, 0);
	return 0;
}

// Delete newline characters in a string (and carriage returns.)
void removeNewlines(char *str) {
	int x = 0;
	while (str[x] != 0) {
		if (str[x] == 0xA || str[x] == 0xD) {
			str[x] = 0;
		}
		x++;
	}
}

// Prints the current commands
void printCommands() {
    struct dictStruct *command;

    for(command=waitingCommands; command != NULL; command=command->hh.next) {
        printf("Client fd %d: Command  '%s'\n", command->client_fd, command->data);
    }
}

// Remove a command from the list
void popCommand(int fileDescriptor) {
    struct dictStruct *command;

    HASH_FIND_INT(waitingCommands, &fileDescriptor, command);
    HASH_DEL(waitingCommands, command);
}

// Get the next command from the list
// Pass it a buffer to fill with the text
// Returns the file descriptor if successful
int getNextCommand(char *str) {
	struct dictStruct *command;

    for(command=waitingCommands; command != NULL; command=command->hh.next) {
        strcpy(str, command->data);
        return command->client_fd;
    }

	return 0;
}

// Adds a new command to the list waiting to be handled.
void addCommand(int fileDescriptor, char* data) {
	struct dictStruct *s;
	struct dictStruct *replaced; // yes, this is discarded for now..

	s = malloc(sizeof(struct dictStruct));
	s->client_fd = fileDescriptor;
	strcpy(s->data, data);

	HASH_REPLACE_INT(waitingCommands, client_fd, s, replaced);
}

// Accepts and/or closes client connections
int handleClients() {
	// Update our file descriptor sets, poll style
	// (non blocking, timeout = 0)
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	read_fds = master_fds; // Copy the master fds real quick
	if (select(fdmax+1, &read_fds, NULL, NULL, &timeout) == -1) {
		printf("Error on polling with select \n");
		return -1;
	}

	// Poll the file descriptor set
	for (int i = 0; i <= fdmax; i++) {
		if (FD_ISSET(i, &read_fds)) {
			// Handle new connections from the listener
			if (i == listener) {
				addrLen = sizeof clientAddr;
				clientSock = accept(listener, &clientAddr, &addrLen);

				if (clientSock == -1) {
					printf("Error on accepting client!\n");
				} else {
					// Add client to file descriptor set
					FD_SET(clientSock, &master_fds);

					// Re-calculate max
					if (clientSock > fdmax) {
						fdmax = clientSock;
					}
					clientAddrButEasier = (struct sockaddr_in *) &clientAddr;
					inet_ntop(AF_INET, &(clientAddrButEasier->sin_addr), ipStr, sizeof ipStr);
					printf("Server: Got a new client of the ip address: %s\n", ipStr);
				}
			} else {
				// A client sent data or closed a connection
				memset(buf, 0, sizeof buf); // Reset buffer to 0s
				if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) {
					if (nbytes == 0) {
						printf("Socket %d hung up.\n", i);
					} else {
						printf("Reciever error.\n");
					}
					close(i); // Close the broken socket.
					FD_CLR(i, &master_fds); // remove from set
				} else {
					// We just got some data.

					// cleanse the newlines! (Mostly from testing applications like telnet.
					removeNewlines(buf);

					printf("Recieved command '%s'\n", buf);

					// Add the message and the client descriptor to the hashtable
					addCommand(i, buf);
				}
			}
		}
	}

	return 0;
}


// Setup our listening port
// Should only be called once.
// Returns -1 if failure occurs
int setupListener() {
	//socklen_t addr_size;
	struct addrinfo hints, *res;
	int yes = 1;

	// NULL the hashtable
	waitingCommands = NULL;

	// Zero out file descriptor sets
	FD_ZERO(&master_fds);
	FD_ZERO(&read_fds);

	// Setup struct
	memset(&hints,0,sizeof hints);
	hints.ai_family = AF_UNSPEC; // Unspecified
	hints.ai_socktype = SOCK_STREAM; // TCP.
	hints.ai_flags = AI_PASSIVE; // fill in my IP for me.

	getaddrinfo(NULL, "51717", &hints, &res);

	// Setup a network socket and listen on port 51717
	listener = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

	// We have a network descriptor now.
	if (listener == -1) {
		printf("Failed to make a sock.");
		close(listener);
		exit(1);
	}

	// This disables blocking while waiting on data.
	fcntl(listener, F_SETFL, O_NONBLOCK);

	// Supposed to remove the "already in use" error message
	setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int));

	// Bind to our port.
	if (bind(listener, res->ai_addr, res->ai_addrlen) == -1){
		printf("Error'd on bind.");
		close(listener);
		exit(1);
	}

	// Listen here...
	if (listen(listener, 10) == -1) {
		close(listener);
		return -1;
	}

	// Add listener to master set
	FD_SET(listener, &master_fds);

	// fdmax starts out as the listener (since it's the only one in the beginning)
	fdmax = listener;

	printf("Listener established.\n");
	return 0;
}

/*
int getConnections() {
	// Network code was learned/copied from
	// https://beej.us/guide/bgnet/
	// (Mostly learned, my structuring is a bit different)
	socklen_t addr_size;
	struct addrinfo hints, *res;
	struct sockaddr theirAddr;
	struct sockaddr_in *theirAddrButEasier;
	int mySock, theirSock;
	int yes = 1;
	char s[INET6_ADDRSTRLEN];
	char *dataBuffer[32];

	//memset(*dataBuffer, 0, 32);

	// Setup struct
	memset(&hints,0,sizeof hints);
	hints.ai_family = AF_UNSPEC; // Unspecified
	hints.ai_socktype = SOCK_STREAM; // TCP.
	hints.ai_flags = AI_PASSIVE; // fill in my IP for me.

	getaddrinfo(NULL, "51717", &hints, &res);

	// Setup a network socket and listen on port 51717
	mySock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

	// We have a network descriptor now.
	if (mySock == -1) {
		printf("Failed to make a sock.");
		close(mySock);
		exit(1);
	}

	// This disables blocking while waiting on data.
	fcntl(mySock, F_SETFL, O_NONBLOCK);

	// Supposed to remove the "already in use" error message
	setsockopt(mySock, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int));

	// Bind to our port.
	if (bind(mySock, res->ai_addr, res->ai_addrlen) == -1){
		printf("Error'd on bind.");
		close(mySock);
		exit(1);
	}

	// Listen for new connections.
	if (listen(mySock, 5) == -1) {
		printf("Error'd on listen");
		close(mySock);
		exit(1);
	}

	printf("Waiting for connections...\n");

	int result;
	for (int i = 0; i < 50; i++) {
		// Accept new connections
		addr_size = sizeof theirAddr;
		theirSock = accept(mySock, &theirAddr, &addr_size);

		if (theirSock == -1) {
			usleep(100000);
			continue;
		}

		// No capes! Er, I mean, no blocking!
		fcntl(theirSock, F_SETFL, O_NONBLOCK);

		theirAddrButEasier = (struct sockaddr_in *) &theirAddr;
		inet_ntop(AF_INET, &(theirAddrButEasier->sin_addr), s, sizeof s);
		printf("server: got connection from %s\n", s);

		// Have I ever mentioned how much I hate C and C++ pointers? Like, it seems
		// **ESPECIALLY** obtuse, especially with the whole type casting thing.
		// I hope Rust's system is better.


		result = recv(theirSock, dataBuffer, 31, 0);
		if (result == -1) {
			printf("Failed to retrieve data?\n");
		} else {
			printf("Received %d: %s\n", result, dataBuffer);
		}
		close(theirSock);

	}
	close(mySock);
	return 0;
} */
