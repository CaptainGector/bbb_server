/*
 * network.h
 *
 *  Created on: Nov 13, 2020
 *      Author: gector
 */

#ifndef SRC_NETWORK_H_
#define SRC_NETWORK_H_

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include "uthash.h"


	// Network code was learned/copied from
	// https://beej.us/guide/bgnet/
	// (Mostly learned, my structuring is a bit different)

// Function prototypes
void addCommand(int fileDescriptor, char* data);
void printCommands();
int getNextCommand(char *str);
void popCommand(int fileDescriptor);
int handleClients();
int setupListener();
int getMessages();
int sendToClient(int fd, char *str, int len);

fd_set master_fds; 			// Master file descriptor list (includes listener
fd_set read_fds; 			// Our ready file descriptors
int fdmax;					// Maxinum number of file descriptors
struct timeval timeout; 	// Our timeout struct for the select() function

socklen_t addrLen;
struct sockaddr clientAddr;
struct sockaddr_in *clientAddrButEasier;

int nbytes;
char buf[256]; 		// Input buffer from clients.
char ipStr[INET6_ADDRSTRLEN];

int listener;		// File descriptor for the listener
int clientSock;		// A file descriptor for new clients

// Hash (dictionary) stuff.
struct dictStruct {
    int client_fd;                    /* key */
    char data[100];
    UT_hash_handle hh;         /* makes this structure hashable */
};

struct dictStruct *waitingCommands;// = NULL;    /* important! initialize to NULL */

#endif /* SRC_NETWORK_H_ */
