/*
 * sensor.h
 *
 *  Created on: Nov 13, 2020
 *      Author: Zachary Whitlock
 */

#ifndef SRC_SENSOR_H_
#define SRC_SENSOR_H_

#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdio.h>


int initAccel(int fileDesc);
int readAxis(int fileDescriptor, char regAddr);
int writeRegister(int fileDescriptor, char regAddr, char data);
int readRegister(int fileDescriptor, char regAddr);

#endif /* SRC_SENSOR_H_ */
