/*
 * motor.c
 *
 *  Created on: Nov 13, 2020
 *      Author: Zachary Whitlock
 */

#include "motor.h"

const char CCW[8] = {0x09,0x01,0x03,0x02,0x06,0x04,0x0c,0x08};  //CouterClockWise
const char CW[8]= {0x08,0x0c,0x04,0x06,0x02,0x03,0x01,0x09};    //ClockWise
const char motorPins[4] = {8, 10, 12, 14};

// Find an element of an array
// returns -1 on fail, index if good
int indexOf(const char *array, char searchFor, int len) {
	for(int x = 0; x < len; x++){
		if (array[x] == searchFor) {
			return x;
		}
	}
	return -1;
}
// Function to simplify writing GPIO
void setPin(char port, char pin, uint8_t state) {
	if (state) // 0 = OFF, else = ON
		pin_high(port, pin);
	else
		pin_low(port, pin);
}

// Function for easily controlling the 4 GPIO pins
int writeNibbleToGPIO(char data, int delay) {
	// Write GPIO to the lower 4 bits of 'data'
	setPin(MOTOR_PORT, motorPins[0], 0x01 & data);
	setPin(MOTOR_PORT, motorPins[1], 0x02 & data);
	setPin(MOTOR_PORT, motorPins[2], 0x04 & data);
	setPin(MOTOR_PORT, motorPins[3], 0x08 & data);
	usleep(delay);

	return 0;
}

// Step the motor once (DRAFT)
// dir 0 = CCW
int stepMotor(int dir) {
	int lastIndex;

	if (dir == 0) { // CCW
		lastIndex = indexOf(CCW, lastStep, sizeof(CCW));
		if (lastIndex < 0){
			lastStep = CCW[0];
		}

		// Goto beginning again
		if (lastIndex >= (sizeof(CCW)-1)) {
			lastIndex = 0;
			writeNibbleToGPIO(CCW[lastIndex], MOTOR_DELAY);
		} else {
			lastIndex++;
			writeNibbleToGPIO(CCW[lastIndex], MOTOR_DELAY);
		}

		lastStep = CCW[lastIndex];
		//printf("Last index: %X\n", lastStep);

	} else {		// CW
		lastIndex = indexOf(CW, lastStep, sizeof(CW));
		if (lastIndex < 0)
			return -1;

		// Goto beginning again
		if (lastIndex >= (sizeof(CW)-1)) {
			lastIndex = 0;
			writeNibbleToGPIO(CW[lastIndex], MOTOR_DELAY);
		} else {
			lastIndex++;
			writeNibbleToGPIO(CW[lastIndex], MOTOR_DELAY);
		}
		lastStep = CW[lastIndex];
	}

    writeNibbleToGPIO(0x00, 0);
	return 0;
}

int stepMotorN(int numSteps, int dir) {
	printf("STEPPING %d\n", numSteps);

	if (numSteps <= 0) {
		printf("Motor needs positive steps.\n");
		return -1;
	}

	for (int x = 0; x < numSteps; x++) {
		stepMotor(dir);
	}

    // Turn off GPIO
    writeNibbleToGPIO(0x00, 0);

	return 0;
}
