/*
 * motor.h
 *
 *  Created on: Nov 13, 2020
 *      Author: Zachary Whitlock
 */

#ifndef SRC_MOTOR_H_
#define SRC_MOTOR_H_

#include <iobb.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#define MOTOR_PORT 8	// Connector #
#define MOTOR_DELAY 2000 // uS

void setPin(char port, char pin, uint8_t state);
int indexOf(const char *array, char searchFor, int len);
int writeNibbleToGPIO(char data, int delay);
int stepMotor(int dir);
int stepMotorN(int numSteps, int dir);

// Keeps track of the last step we did
char lastStep;

#endif /* SRC_MOTOR_H_ */

